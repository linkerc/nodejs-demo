var http = require('http');
var url = require('url');
var cp = require('child_process');

function onRequest(request, response) {
    var pathname = url.parse(request.url).pathname;
    if( pathname == '/wait' ){
        cp.exec('node blocking.js', myCallback);
    }
    else{
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write('Hello!\n');
        response.end();
    }

    console.log('New connection');

    function myCallback(err,stdout,stderr){
        response.writeHead(200, {'Content-Type': 'text/plain'});
        response.write('1212Thanks for waiting!\n');
		console.log("child process failed with error code: " );
        response.end();
    }
}
http.createServer(onRequest).listen(8080);
console.log('Server started');